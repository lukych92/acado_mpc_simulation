#include <acado_gnuplot.hpp>
#include <acado_toolkit.hpp>
#include <fstream>
#include <math.h>
#include <random>

int main( )
{
    USING_NAMESPACE_ACADO

//    PARAMATERS
    StaticReferenceTrajectory zeroReference("data/reference_trajectory.dat");

    double Tp   =   0.5;
    double Tc   =   0.01;
    double T0   =   0.0;
    double Tt   =   2*M_PI;
    int num_steps = 1;
    const int N = 50;

    double q0_1 = 0.0;
    double q0_2 = 0.0;
    double q0_3 = 0.0;
    double q0_4 = 0.0;

    double u0_1 = 1.0;
    double u0_2 = 0.0;

    double distQ1 = 0.5;
    double distQ2 = 0.5;
    double distQ3 = 10*M_PI/180;
    double distQ4 = 0;

    int no_of_states    = 4;
    int no_of_controls  = 2;

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> distq1(-distQ1, distQ1);
    std::uniform_real_distribution<double> distq2(-distQ2, distQ2);
    std::uniform_real_distribution<double> distq3(-distQ3, distQ3);
    std::uniform_real_distribution<double> distq4(-distQ4, distQ4);

    DVector disturbances(no_of_states);


//     Define the dynamic system:
//     -------------------------
    DifferentialState     q1,q2,q3,q4;
    Control               u1,u2;

//    Simple kinematic car
    DifferentialEquation  f;
    f << dot(q1) == u1*cos(q3)*cos(q4);
    f << dot(q2) == u1*sin(q3)*cos(q4);
    f << dot(q3) == u1*sin(q4);
    f << dot(q4) == u2;

// DEFINE LEAST SQUARE FUNCTION:
    Function h, hN;

    h << q1 << q2 << q3 << u1 << u2;
    hN << q1 << q2 << q3;

    DMatrix Q = eye<double>( h.getDim() );
    DMatrix QN = eye<double>( hN.getDim() );

    Q(0,0)=QN(0,0)=100;
    Q(1,1)=QN(1,1)=100;
    Q(2,2)=QN(2,2)=100;
    Q(3,3) = 0.005;
    Q(4,4) = 0.005;
//    --------------------------------------------------------------------
   // DEFINE AN OPTIMAL CONTROL PROBLEM
    DVector r(no_of_states);
    r.setAll( 0.0 );

    OCP ocp        ( T0, Tp, N);
    ocp.minimizeLSQ(Q, h, r);
    ocp.minimizeLSQEndTerm(QN, hN);
    ocp.subjectTo  ( f );

    // SETTING UP THE (SIMULATED) PROCESS:
    // -----------------------------------
    OutputFcn identity;
    DynamicSystem dynamicSystem( f,identity );

    Process process( dynamicSystem, INT_RK45 );

    // SETTING UP THE MPC CONTROLLER:
    // ------------------------------


    RealTimeAlgorithm alg( ocp, Tc );
    alg.set( MAX_NUM_ITERATIONS, num_steps );


    Controller controller( alg, zeroReference );

    VariablesGrid feedbackControl;
    VariablesGrid sampledProcessOutput;


    // SETTING UP THE SIMULATION ENVIRONMENT,  RUN THE EXAMPLE...
    // ----------------------------------------------------------
    DVector x0(no_of_states);
    x0(0) = q0_1;
    x0(1) = q0_2;
    x0(2) = q0_3;
    x0(3) = q0_4;


    DVector uCon(no_of_controls);
    uCon(0) = u0_1;
    uCon(1) = u0_2;

    VariablesGrid ySim;
    DVector actualY;

    int nSteps = 0;
    double currentTime = T0;

    if (controller.init( T0,x0 ) != SUCCESSFUL_RETURN)
        exit( EXIT_FAILURE );
//    controller.getU( uCon );

    if (process.init( T0,x0,uCon ) != SUCCESSFUL_RETURN)
        exit( EXIT_FAILURE );
    process.getY( ySim );


    while ( currentTime <= Tt )
    {
        printf( "\n*** Simulation Loop No. %d (starting at time %.3f) ***\n",nSteps,currentTime );

        feedbackControl.addVector(uCon, currentTime);
        sampledProcessOutput.addVector(ySim.getLastVector(), currentTime);

        actualY = ySim.getLastVector();
        disturbances[0] = distq1(mt);
        disturbances[1] = distq2(mt);
        disturbances[2] = distq3(mt);
        disturbances[3] = distq4(mt);
        actualY += disturbances;

        std::cout << "Disturbances " << disturbances << std::endl;




        if (controller.step( currentTime,actualY ) != SUCCESSFUL_RETURN)
            exit( EXIT_FAILURE );
        controller.getU( uCon );

        if (process.step( currentTime,currentTime+Tc,uCon ) != SUCCESSFUL_RETURN)
            exit( EXIT_FAILURE );
        process.getY( ySim );

        ++nSteps;
        currentTime = (double)nSteps * Tc;

    }


    // SAVE RESULTS
    // ----------------------------------------------------------
    sampledProcessOutput.print("data/states.dat");
    feedbackControl.print("data/controls.dat");

//
//    GnuplotWindow window;
//    GnuplotWindow window1;
//    window.addSubplot( sampledProcessOutput(0),   "q1" );
//    window.addSubplot( sampledProcessOutput(1),   "q2" );
//    window.addSubplot( sampledProcessOutput(2),   "q3" );
//    window1.addSubplot( feedbackControl(0), "u1" );
//    window1.addSubplot( feedbackControl(1), "u2" );
//    window.plot();
//    window1.plot();

    return EXIT_SUCCESS;

}



