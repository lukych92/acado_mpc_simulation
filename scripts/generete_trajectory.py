#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt

def frange(x, y, jump):
    while x < y:
        yield x
        x += jump


def generete_lissajous(param):
    T =  param['T']
    dts = param['dts']
    fname = param['fname']


    ## desired trajectory
    R  = param['R']
    # w = param['w']

    time = []
    reference_x = []
    reference_y = []
    reference_orientation = []
    # velocity_linear = []
    # velocity_angular = []

    # for t in np.linspace(0,T,314*4):
    for t in [x for x in frange(0, T, dts)]:
        time.append(t)
        # orientation = (w*t)-np.pi/2
        reference_x.append(R*np.sin(t+np.pi/2))
        reference_y.append(R*np.sin(2*t))
        reference_orientation.append(np.arctan2(2*R*np.cos(2*t),R*np.cos(t+np.pi/2))+r(t))
        # velocity_linear.append(param['vel_linear'])
        # velocity_angular.append(param['vel_angular'])

    if param['vis'] == True:
        plt.plot(reference_x,reference_y,'k--')
        plt.show()

        plt.plot(time, reference_x,'.')
        plt.show()

        plt.plot(time, reference_y,'.')
        plt.show()

        plt.plot(time, reference_orientation,'.')
        plt.show()


    f= open(fname, 'w')

    for i in range(len(time)):
        string_line = ' '.join(map(str,[time[i],reference_x[i],reference_y[i],reference_orientation[i]]))
        f.write(string_line+'\n')

def r(x):
    y = x
    if np.pi/4 <= y and y < 3*np.pi/4:
        return 2*np.pi
    else:
        return 0

if __name__ == "__main__":
    param = {}
    param['T'] = 2*np.pi
    param['R'] = 5
    param['dts'] = 0.01
    param['fname'] = "data/reference_trajectory.dat"
    param['vis'] = False

    generete_lissajous(param)



