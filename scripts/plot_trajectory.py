#!/usr/bin/python

import matplotlib.pyplot as plt
import sys
import numpy as np


def plot_from(fname, fig_name):
    lines = tuple(open(fname, 'r'))
    lines = [x.strip() for x in lines]

    comment = ""
    time = []
    actual_x = []
    actual_y = []
    actual_orientation = []
    reference_x = []
    reference_y = []
    reference_orientation = []
    velocity_linear = []
    velocity_angular = []


    for i in range(len(lines)):
        if lines[i][0] == '#':
            comment += lines[i]+str('\n')
        else:
            # convert to integer and append to the last
            # element of the list
            line = lines[i].split()
            time.append(float(line[0]))
            actual_x.append(float(line[1]))
            actual_y.append(float(line[2]))
            actual_orientation.append(float(line[3]))
            reference_x.append(float(line[4]))
            reference_y.append(float(line[5]))
            reference_orientation.append(float(line[6]))
            velocity_linear.append(float(line[7]))
            velocity_angular.append(float(line[8]))


    # print(comment)
    # print("Do you want to save figures to files? (T/n)")
    # save_to_file = raw_input()
    save_to_file = 'T'
    fig_name = 'figures/'+fig_name
    # if save_to_file == 'T' or save_to_file == '':
    #     print("Give the name, please")
    #     fig_name = raw_input()

    plt.plot(reference_x,reference_y,'k--',label='desired')
    plt.plot(actual_x,actual_y,'k',label = 'real')
    plt.xlim([-6, 6])
    plt.ylim([-6, 6])
    # plt.plot(actual_x[0],actual_y[0],'ok', label="Initial position")
    # plt.title('Desired and realized path')
    plt.legend(loc=9, numpoints=1)
    plt.xlabel('$q_1$ [m]')
    plt.ylabel('$q_2$ [m]')
    if save_to_file == 'T' or save_to_file == '':
        plt.savefig(fig_name+".png",
                    format = 'png'
                    )
        plt.clf()
    else:
        plt.show()

    # mse_x = 100*np.sum((np.array(reference_x)-np.array(actual_x))**2)/len(reference_x)
    plt.subplot(311)
    plt.plot(time,np.array(actual_x)-np.array(reference_x),'k',label='theta')
    plt.xlim([0, time[-1]])
    plt.xlabel('t [s]')
    plt.ylabel('$e_1$ ')
    # if save_to_file == 'T' or save_to_file == '':
    #     plt.savefig(fig_name+"_e1.png",
    #                 format = 'png'
    #                 )
    #     plt.clf()
    # else:
    #     plt.show()

    # mse_y = 100*np.sum((np.array(reference_y)-np.array(actual_y))**2)/len(reference_y)
    plt.subplot(312)
    plt.plot(time,np.array(actual_y)-np.array(reference_y),'k',label='theta')
    plt.xlim([0, time[-1]])
    plt.xlabel('t [s]')
    plt.ylabel('$e_2$ ')
    # if save_to_file == 'T' or save_to_file == '':
    #     plt.savefig(fig_name+"_e2.png",
    #                 format = 'png'
    #                 )
    #     plt.clf()
    # else:
    #     plt.show()

    # mse_orientation = 100*np.sum((np.array(reference_orientation)-np.array(actual_orientation))**2)/len(reference_orientation)
    plt.subplot(313)
    plt.plot(time,np.array(actual_orientation)-np.array(reference_orientation),'k',label='theta')
    plt.xlim([0, time[-1]])
    plt.xlabel('t [s]')
    plt.ylabel('$e_3$ ')
    if save_to_file == 'T' or save_to_file == '':
        plt.savefig(fig_name+"_errors.png",
                    format = 'png'
                    )
        plt.clf()
    else:
        plt.show()

    plt.plot(time,velocity_linear,'k',label='velocity_linear')
    plt.xlim([0, time[-1]])
    plt.xlabel('t [s]')
    plt.ylabel('$u_1$')
    if save_to_file == 'T' or save_to_file == '':
        plt.savefig(fig_name+"_u1.png",
                    format = 'png'
                    )
        plt.clf()
    else:
        plt.show()


    plt.plot(time,velocity_angular,'k',label='velocity_angular')
    plt.xlabel('t [s]')
    plt.ylabel('$u_2$')
    plt.xlim([0, time[-1]])
    if save_to_file == 'T' or save_to_file == '':
        plt.savefig(fig_name+"_u2.png",
                    format = 'png'
                    )
        plt.clf()
    else:
        plt.show()

if __name__ == "__main__":

    if len(sys.argv)>1:
        fname = sys.argv[1]
        fig_name = sys.argv[2]
        plot_from(fname,fig_name)
    else:
        print("Get file name as a first argument, please!")
