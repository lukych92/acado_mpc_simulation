#!/usr/bin/python

import matplotlib.pyplot as plt
import sys
import numpy as np


def combine_from(states, reference, controls, fname):
    time = []
    actual_x = []
    actual_y = []
    actual_orientation = []
    reference_x = []
    reference_y = []
    reference_orientation = []
    velocity_linear = []
    velocity_angular = []


    lines_states = tuple(open(states, 'r'))
    lines_states = [x.strip() for x in lines_states]

    for i in range(len(lines_states)):
        if lines_states[i][0] == '#':
            comment += lines[i]+str('\n')
        else:
            # convert to integer and append to the last
            # element of the list
            line = lines_states[i].split()
            time.append(float(line[1]))
            actual_x.append(float(line[2]))
            actual_y.append(float(line[3]))
            actual_orientation.append(float(line[4]))

    lines_reference = tuple(open(reference, 'r'))
    lines_reference = [x.strip() for x in lines_reference]

    for i in range(len(lines_reference)):
        if lines_reference[i][0] == '#':
            comment += lines[i]+str('\n')
        else:
            # convert to integer and append to the last
            # element of the list
            line = lines_reference[i].split()
            reference_x.append(float(line[1]))
            reference_y.append(float(line[2]))
            reference_orientation.append(float(line[3]))

    lines_controls = tuple(open(controls, 'r'))
    lines_controls = [x.strip() for x in lines_controls]

    for i in range(len(lines_controls)):
        if lines_controls[i][0] == '#':
            comment += lines[i]+str('\n')
        else:
            # convert to integer and append to the last
            # element of the list
            line = lines_controls[i].split()
            velocity_linear.append(float(line[2]))
            velocity_angular.append(float(line[3]))

    f= open("data/"+fname+".dat", 'w')

    for i in range(len(time)):
        string_line = ' '.join(map(str,[time[i],
                                        actual_x[i],
                                        actual_y[i],
                                        actual_orientation[i],
                                        reference_x[i],
                                        reference_y[i],
                                        reference_orientation[i],
                                        velocity_linear[i],
                                        velocity_angular[i]]
                                   )
                               )
        f.write(string_line+'\n')

if __name__ == "__main__":

    if len(sys.argv)>1:
        fname = sys.argv[1]

    combine_from("data/states.dat","data/reference_trajectory.dat","data/controls.dat", fname)

    # if len(sys.argv)>1:
    #     fname = sys.argv[1]
    # else:
    #     print("Get file name as a first argument, please!")
