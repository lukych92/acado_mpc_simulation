#!/bin/bash

./make_clean.sh

./scripts/generete_trajectory.py
cmake --build build/ --target ex1 -- -j4
cmake --build build/ --target ex2 -- -j4
cmake --build build/ --target ex3 -- -j4
cmake --build build/ --target ex4 -- -j4
cmake --build build/ --target ex5 -- -j4


echo "Started ex1"
./ex1 > ex1.log
./scripts/combine_file_to_print.py ex1
./scripts/plot_trajectory.py data/ex1.dat ex1
#eog figures/ex1.png

echo "Started ex2"
./ex2 > ex2.log
./scripts/combine_file_to_print.py ex2
./scripts/plot_trajectory.py data/ex2.dat ex2
#eog figures/ex2.png

echo "Started ex3"
./ex3 > ex3.log
./scripts/combine_file_to_print.py ex3
./scripts/plot_trajectory.py data/ex3.dat ex3
#eog figures/ex3.png

echo "Started ex4"
./ex4 > ex4.log
./scripts/combine_file_to_print.py ex4
./scripts/plot_trajectory.py data/ex4.dat ex4
#eog figures/ex4.png

echo "Started ex5"
./ex5 > ex5.log
./scripts/combine_file_to_print.py ex5
./scripts/plot_trajectory.py data/ex5.dat ex5
#eog figures/ex5.png