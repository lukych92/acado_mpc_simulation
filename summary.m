function summary(name)

  sym_i = 1;
  vlen  = 0.1;
  skip  = 4;

  XLim  = [-6, 6];
  YLim  = [-6, 6];

  %if exist([dir, 'Sym', int2str(sym_i)], 'dir') == 7
  %  rmdir([dir, 'Sym', int2str(sym_i)],'s');
  %end
  %mkdir([dir, 'Sym', int2str(sym_i)]);
  %
  %load([dir,nazwa,'_',ptype,'_',atype,'_sym',int2str(sym_i),'_pre.mat']);

  prm.dir = 'figures';
  prm.sym_i = 1;
  prm.nazwa = name;
  prm.ptype = "0";
  prm.atype = "0";
  prm.plot.name = "";


  data = load(strcat('data/',name,'.dat'));

  t   = data(:,1);
  q   = data(:,2:4);%out.q;
  yd  = data(:,5:7);%out.yd;
  u   = data(:,8:9);%out.u;

  dim.r = 3;


  dim.t = length(t);
  e   = zeros(dim.t, dim.r);
  for i = 1 : dim.t
    e(i, :) = q(i,:) - yd(i, :);
  end


  fig1 = figure;
  ax1  = axes;
  plot(ax1, yd(:, 1), yd(:, 2), 'b--', 'LineWidth', 0.3);
  hold(ax1, 'on');
  plot(ax1, q(:, 1), q(:, 2), 'k', 'LineWidth', 0.5);
  set(ax1,'XLim', XLim, 'YLim', YLim);
  xlabel(ax1, 'q_1 [m]','FontSize',20);
  ylabel(ax1, 'q_2 [m]','FontSize',20);
  legend('desired', 'real', 'Location', 'North');
  set(ax1,'FontSize',18);
  print(fig1, '-depsc','-r1200',[prm.dir, '/', ...
    prm.nazwa, '_', prm.ptype, '_', prm.atype, '_sym', int2str(prm.sym_i), ...
    '_final_path', prm.plot.name]);

  fig2a = figure;
  ax2a  = axes;
  hold(ax2a, 'on');
  h_u1 = plot(ax2a, t, u(:, 1),  'LineWidth', 1);
  box(ax2a, 'on');
  hold(ax2a, 'off');
  axis tight;
  xlabel(ax2a, 't [s]','FontSize',20);
  ylabel(ax2a, 'u_1','FontSize',20);
  set(ax2a,'FontSize',18);
  print(fig2a, '-depsc','-r1200',[prm.dir, '/', ...
    prm.nazwa, '_', prm.ptype, '_', prm.atype, '_sym', int2str(prm.sym_i), ...
    '_u1', prm.plot.name]);

  fig2b = figure;
  ax2b  = axes;
  hold(ax2b, 'on');
  h_u1 = plot(ax2b, t, u(:, 2),  'LineWidth', 1);
  box(ax2b, 'on');
  hold(ax2b, 'off');
  axis tight;
  xlabel(ax2b, 't [s]','FontSize',20);
  ylabel(ax2b, 'u_2','FontSize',20);
  set(ax2b,'FontSize',18);
  print(fig2b, '-depsc','-r1200',[prm.dir, '/', ...
    prm.nazwa, '_', prm.ptype, '_', prm.atype, '_sym', int2str(prm.sym_i), ...
    '_u2', prm.plot.name]);

  fig3 = figure;
  %ax3  = axes;
  ax3a = subplot(3,1,1);
  plot(ax3a, t, e(:, 1), 'LineWidth', 1);
  ax3b = subplot(3,1,2);
  plot(ax3b, t, e(:, 2), 'LineWidth', 1);
  ax3c = subplot(3,1,3);
  plot(ax3c, t, e(:, 3), 'LineWidth', 1);
  axis([ax3a, ax3b, ax3c],'tight');

  %[ax3yy,h1,h2] = plotyy(ax3, t, e(:, 1:2), t, e(:, 3),'plot');
  % plot(ax3, t, e(:, 1), t, e(:, 2));
  % plot(ax3, t, e(:, 3));
  %axis(ax3yy,'tight');
  %axis(ax3,'tight');
  xlabel(ax3c, 't [s]','FontSize',20);
  ylabel(ax3a, 'e_1','FontSize',20);
  ylabel(ax3b, 'e_2','FontSize',20);
  ylabel(ax3c, 'e_3','FontSize',20);
  set(ax3a,'FontSize',18);
  set(ax3b,'FontSize',18);
  set(ax3c,'FontSize',18);
  %set(get(ax3yy(1),'Ylabel'),'String','e_1, e_2 [m]') 
  %set(get(ax3yy(2),'Ylabel'),'String','e_3 [rad]') 
  %ylabel(ax3, 'e_1, e_2','FontSize',20);
  %set(ax3yy(1),'FontSize',18);
  %set(ax3yy(2),'FontSize',18);
  print(fig3, '-depsc','-r1200',[prm.dir, 'Sym', int2str(prm.sym_i), '/', ...
    prm.nazwa, '_', prm.ptype, '_', prm.atype, '_sym', int2str(prm.sym_i), ...
    '_e', prm.plot.name]);

