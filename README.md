# README #

Example of model predictive control simulation. Kinematic car model is used.
---
Init repository

```
#!bash

./make_init.sh
```
---
Start simulation

```
#!bash

./make.sh
```
---
Clean repository

```
#!bash

./make_clean.sh
```
